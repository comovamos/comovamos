angular.module('app')


.controller('MenuController', ['$scope', '$state', function($scope, $state) {

	$scope.menuCtrl = {
    	menu: [{
    		name: "Promesas",
    		state: "promesas",
            activeFor: ["promesas"],
            icon: "icon-th-list",
    		href: "#/" 
    	}]
    }

    $scope.isActive = function(menuItem) {

        var parentstate = $state.current.name.split('.');
        parentstate = parentstate[0];
        
    	if(_.indexOf(menuItem.activeFor, parentstate) >= 0) {
    		return "active";
    	}
    }

}])