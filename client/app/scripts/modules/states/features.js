angular.module('cincuenta', ['promesas','ParseServices', 'ExternalDataServices'])

.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {

	$stateProvider

	.state('promesas.cincuenta', {
		abstract: true
    })
    .state('promesas.cincuenta.list', {
    	url: '/cincuenta',
    	views: {
    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/cincuenta.list.html'
    		}

    	}
    })

    .state('promesas.cincuenta.list.facebook', {
    	url: '/facebook',
    	views: {
    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/features.list.facebook.html'
    		}

    	}
    })

    .state('promesas.cincuenta.list.facebookSDK', {
        url: '/facebookSDK',
        views: {
            'detail@promesas' : {
                templateUrl: 'app/views/detail/features.list.facebookSDK.html'
            }

        }
    })

    .state('promesas.cincuenta.list.parse', {
    	url: '/parse',
    	views: {
    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/features.list.parse.html'
    		}
    	}
    })

    .state('promesas.cincuenta.list.cloudCode', {
        url: '/cloud-code',
        views: {
            'detail@promesas' : {
                templateUrl: 'app/views/detail/features.list.cloud-code.html'
            }
        }
    })

    .state('promesas.cincuenta.list.parseSDK', {
    	url: '/parse-sdk',
    	views: {
    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/features.list.parse-sdk.html'
    		}
    	}
    })

    .state('promesas.cincuenta.list.educacion', {
    	url: '/educacion',
    	views: {
    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/cincuenta.list.educacion.html'
    		}
    	}
    })
	

}])