angular.module('promesas', ['ParseServices', 'ExternalDataServices'])

.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {

	$stateProvider

	.state('promesas', {
		abstract: true,
        views: {
            '@': {
                templateUrl: 'app/views/app-layout.html',
            },
            'panel@promesas': {
                templateUrl: 'app/views/master-detail.html',
                controller: 'MasterDetailController',
                resolve: {
                    'monsters': ['MonsterService', function(MonsterService) {

                        // get the collection from our data definitions
                        var monsters = new MonsterService.collection;

                        // use the extended Parse SDK to load the whole collection
                        return monsters.load();

                    }]
                }

            }
        }
    })
    .state('promesas.crud', {
    	url: '/',
    	views: {
    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/crud.list.html'
    		}

    	}
    })

    .state('promesas.crud.detail', {
    	url: 'crud/{monsterId}',
    	views: {
    		
    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/crud.detail.html',
                controller: 'DetailController'
    		}

    	}
    })

    .state('promesas.crud.detail.edit', {
        url: '/edit',
        views: {
            'detail@promesas' : {
                templateUrl: 'app/views/detail/crud.detail.edit.html',
                controller: 'DetailController'
            }

        }
    })


}])