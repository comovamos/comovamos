angular.module('about', ['promesas','ParseServices', 'ExternalDataServices'])

.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {

	$stateProvider

	.state('promesas.about', {
		abstract: true
    })
    .state('promesas.about.list', {
    	url: '/about',
    	views: {
    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/about.list.html'
    		}

    	}
    })
    .state('promesas.about.list.state', {
    	url: '/state',
    	views: {

    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/about.list.state.html'
    		}

    	}
    })
    .state('promesas.about.list.animations', {
        url: '/animations',
        views: {

            'detail@promesas' : {
                templateUrl: 'app/views/detail/about.list.animations.html'
            }

        }
    })

    .state('promesas.about.list.theme', {
    	url: '/theme',
    	views: {

    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/about.list.theme.html'
    		}

    	}
    })

    .state('promesas.about.list.bootstrap', {
    	url: '/bootstrap',
    	views: {

    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/about.list.bootstrap.html'
    		}

    	}
    })

    .state('promesas.about.list.fontAwesome', {
    	url: '/font-awesome',
    	views: {

    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/about.list.font-awesome.html'
    		}

    	}
    })

	

}])