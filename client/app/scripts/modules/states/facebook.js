angular.module('facebook', ['ParseServices', 'ExternalDataServices'])

.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {

	$stateProvider

	.state('promesas.facebook', {
		abstract: true,
    	templateUrl: 'app/views/app-layout.html'
    })
    .state('promesas.facebook.example', {
    	url: '/facebook',
    	views: {
    		'detail@promesas' : {
    			templateUrl: 'app/views/detail/facebook.example.html',
                controller: 'FacebookExampleController'
    		}

    	}
    })
	

}])